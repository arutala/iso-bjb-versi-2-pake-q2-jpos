/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package arutala.bjb.switching.iso;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Abbas
 */
public class Bantuan {

    private static String[] args;

    /**
     *
     */
    static String saatIni() {
        DateFormat df = new SimpleDateFormat("MMddHHmmss");
        Date today = Calendar.getInstance().getTime();
        String reportDate = df.format(today);
        return (reportDate);
    }

    String convert_nama(String nama) {
        int panjangnama;
        panjangnama = nama.length();
        if (panjangnama > 35) {
            nama = nama.substring(0, 35);
        } else {
            int selisih = 35 - panjangnama;
            String pengisi_spasi = new String();
            int ulang = 0;
            while (ulang < selisih) {
                pengisi_spasi += " ";
                ulang++;
            }
            nama = nama + pengisi_spasi;

        }
        return nama;
    }

    public static String[] inquiryNOP(String nop, String tahun) {
        // load config file        
        Properties prop = new Properties();
        InputStream input = null;
        int isoConf = 1;

        try {

            input = new FileInputStream("config.isoserver");

            // load a properties file
            prop.load(input);

            // get the property value and print it out
            isoConf = Integer.parseInt(prop.getProperty("dbConf"));

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        if (nop.length() < 18 || tahun.length() < 4) {
            String status = "invalid";
            String data = null;
            return new String[]{status, data};
        }

        String resultString = null;
        int totalBayar = 0;
        Connection con = null;
        PreparedStatement pst = null;
        PreparedStatement pstDenda = null;

        String url = null;
        String user = null;
        String password = null;

        if (isoConf == 1) {
            url = "jdbc:oracle:thin:@//10.0.0.22:1521/ARUTALA";
            user = "PBB_KABBANDUNG";
            password = "semarmesem";
        } else if (isoConf == 2) {
            url = "jdbc:oracle:thin:@//10.0.0.22:1521/ARUTALA";
            user = "PBB_KABMERAUKE";
            password = "semarmesem";
        } else if (isoConf == 3) {
            url = "jdbc:oracle:thin:@//10.0.0.22:1521/DBARUTALA";
            user = "c##kab_bandung";
            password = "semarmesem";
        }

        String ok1 = nop.substring(0, 2);
        String ok2 = nop.substring(2, 4);
        String ok3 = nop.substring(4, 7);
        String ok4 = nop.substring(7, 10);
        String ok5 = nop.substring(10, 13);
        String ok6 = nop.substring(13, 17);
        String ok7 = nop.substring(17, 18);

        try {

            con = DriverManager.getConnection(url, user, password);
            String stm = "SELECT SPPT.NM_WP_SPPT AS NAMAWP, SPPT.JLN_WP_SPPT AS ALAMATWP, SPPT.KELURAHAN_WP_SPPT AS KELURAHANWP, REF_KECAMATAN.NM_KECAMATAN AS KECAMATANWP, SPPT.LUAS_BUMI_SPPT AS LUAS_TANAH, SPPT.LUAS_BNG_SPPT AS LUAS_BANGUNAN, SPPT.TGL_JATUH_TEMPO_SPPT AS TGL_JATUH_TEMPO, SPPT.STATUS_PEMBAYARAN_SPPT AS STATUSBAYAR, SPPT.PBB_YG_HARUS_DIBAYAR_SPPT AS AMOUNTPAJAK FROM SPPT RIGHT JOIN REF_KECAMATAN ON SPPT.KD_KECAMATAN = REF_KECAMATAN.KD_KECAMATAN WHERE SPPT.KD_PROPINSI = ? AND SPPT.KD_DATI2 = ? AND SPPT.KD_KECAMATAN = ? AND SPPT.KD_KELURAHAN = ? AND SPPT.KD_BLOK = ? AND SPPT.NO_URUT = ? AND SPPT.KD_JNS_OP = ? AND SPPT.THN_PAJAK_SPPT = ?";

            pst = con.prepareStatement(stm);

            pst.setString(1, ok1);
            pst.setString(2, ok2);
            pst.setString(3, ok3);
            pst.setString(4, ok4);
            pst.setString(5, ok5);
            pst.setString(6, ok6);
            pst.setString(7, ok7);
            pst.setString(8, tahun);

            ResultSet result = pst.executeQuery();
            while (result.next()) {
                if ("1".equals(result.getString("STATUSBAYAR"))) {
                    String status = "paid";
                    String data = null;
                    return new String[]{status, data};
                } else if (result.getString("NAMAWP").length() == 0) {
                    String status = "notfound";
                    String data = null;
                    return new String[]{status, data};
                }

                String nama = convert_length(result.getString("NAMAWP"), 35);
                String lokasi = convert_length(result.getString("ALAMATWP"), 35);
                String kelurahan = convert_length(result.getString("KELURAHANWP"), 35);
                String kecamatan = convert_length(result.getString("KECAMATANWP"), 35);
                String provinsi = convert_length("JAWA BARAT", 35);
                String luas_tanah = rj_padded0(result.getString("LUAS_TANAH"), 12);
                String luas_bangunan = rj_padded0(result.getString("LUAS_BANGUNAN"), 12);
                String tgljatuhtempo = convert_tempo(result.getString("TGL_JATUH_TEMPO"));
                String tagihan = rj_padded0(result.getString("AMOUNTPAJAK"), 12);

                /*                
                 //hack sementara
                 if (tagihan == null) {
                 String status = "paid";
                 String data = null;
                 return new String[]{status, data};
                 }
                 */
                int denda_sppt = 0;
                String stmDenda = "SELECT HIT_DENDA( PBB_YG_HARUS_DIBAYAR_SPPT,TGL_JATUH_TEMPO_SPPT, SYSDATE) AS DENDA FROM SPPT WHERE SPPT.KD_PROPINSI = ? AND SPPT.KD_DATI2 = ? AND SPPT.KD_KECAMATAN = ? AND SPPT.KD_KELURAHAN = ? AND SPPT.KD_BLOK = ? AND SPPT.NO_URUT = ? AND SPPT.KD_JNS_OP = ? AND SPPT.STATUS_PEMBAYARAN_SPPT = '0' AND SPPT.THN_PAJAK_SPPT = ?";
                pstDenda = con.prepareStatement(stmDenda);
                pstDenda.setString(1, ok1);
                pstDenda.setString(2, ok2);
                pstDenda.setString(3, ok3);
                pstDenda.setString(4, ok4);
                pstDenda.setString(5, ok5);
                pstDenda.setString(6, ok6);
                pstDenda.setString(7, ok7);
                pstDenda.setString(8, tahun);

                ResultSet resultDenda = pstDenda.executeQuery();
                while (resultDenda.next()) {
                    denda_sppt = Integer.parseInt(resultDenda.getString(1));
                }

                String denda = rj_padded0(String.valueOf(denda_sppt), 12);
                totalBayar = (Integer.parseInt(tagihan)) + denda_sppt;
                String Total = rj_padded0(String.valueOf(totalBayar), 12);
                resultString = nop + tahun + nama + lokasi + kelurahan + kecamatan + provinsi + luas_tanah + luas_bangunan + tgljatuhtempo + tagihan + denda + Total;
            }
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(Bantuan.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
            String status = "down";
            String data = null;
            return new String[]{status, data};
        } finally {

            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }

            } catch (SQLException ex) {
                Logger lgr = Logger.getLogger(Bantuan.class.getName());
                lgr.log(Level.SEVERE, ex.getMessage(), ex);
            }
        }
        String status = "success";
        String data = resultString;
        return new String[]{status, data};
    }

    static String convert_length(String nama, int panjang) {
        int panjangnama;
        panjangnama = nama.length();
        if (panjangnama > panjang) {
            nama = nama.substring(0, panjang);
        } else {
            int selisih = 35 - panjangnama;
            //System.out.println("panjangnama: "+panjangnama);
            //System.out.println("selisih: "+selisih);
            int ulang = 0;
            while (ulang < selisih) {
                nama += " ";
                ulang++;
            }
            //System.out.println("nama: \""+nama+"\"");
        }
        return nama;
    }

    static String convert_tempo(String tempo) {
        String YYYY = tempo.substring(0, 4);
        String MM = tempo.substring(5, 7);
        String DD = tempo.substring(8, 10);
        String tempo_baru = YYYY + MM + DD;
        return tempo_baru;
    }

    static String rj_padded0(String bilangan, int panjang) {
        int panjangpadded;
        panjangpadded = bilangan.length();
        if (panjangpadded > 12) {
            bilangan = "more-than-12";
        } else {
            int selisih = 12 - panjangpadded;
            //System.out.println("panjangnama: "+panjangnama);
            //System.out.println("selisih: "+selisih);
            int ulang = 0;
            String padded = new String();
            while (ulang < selisih) {
                padded += "0";
                ulang++;
            }
            bilangan = padded + bilangan;
        }
        return bilangan;
    }

    public static String[] paymentNOP(String nop, String tahun, String totalBayar, String paymentPointCode) {
        // load config file        
        Properties prop = new Properties();
        InputStream input = null;
        int isoConf = 1;

        try {

            input = new FileInputStream("config.isoserver");

            // load a properties file
            prop.load(input);

            // get the property value and print it out
            isoConf = Integer.parseInt(prop.getProperty("dbConf"));

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        if (nop.length() < 18 || tahun.length() < 4) {
            String status = "invalid";
            String data = null;
            return new String[]{status, data};
        }

        Connection con = null;
        PreparedStatement pst = null;

        String url = null;
        String user = null;
        String password = null;

        if (isoConf == 1) {
            url = "jdbc:oracle:thin:@//10.0.0.22:1521/ARUTALA";
            user = "PBB_KABBANDUNG";
            password = "semarmesem";
        } else if (isoConf == 2) {
            url = "jdbc:oracle:thin:@//10.0.0.22:1521/ARUTALA";
            user = "PBB_KABMERAUKE";
            password = "semarmesem";
        } else if (isoConf == 3) {
            url = "jdbc:oracle:thin:@//10.0.0.22:1521/DBARUTALA";
            user = "c##kab_bandung";
            password = "semarmesem";
        }

        String kd_prop = nop.substring(0, 2);
        String kd_dati2 = nop.substring(2, 4);
        String kd_kec = nop.substring(4, 7);
        String kd_kel = nop.substring(7, 10);
        String kd_blok = nop.substring(10, 13);
        String no_urut = nop.substring(13, 17);
        String kd_jns_op = nop.substring(17, 18);

        String[] inquiryduluya = inquiryNOP(nop, tahun);
        int totalTagihan = Integer.parseInt(inquiryduluya[1].substring(254, 265));
        if ("invalid".equals(inquiryduluya[0])) {
            String status = "invalid";
            String data = null;
            return new String[]{status, data};
        } else if ("down".equals(inquiryduluya[0])) {
            String status = "down";
            String data = null;
            return new String[]{status, data};
        } else if ("paid".equals(inquiryduluya[0])) {
            String status = "paid";
            String data = null;
            return new String[]{status, data};
        } else if ("success".equals(inquiryduluya[0]) && (inquiryduluya[1] == null)) {
            String status = "notfound";
            String data = null;
            return new String[]{status, data};
        } else if (inquiryduluya[0] == null) {
            String status = "notfound";
            String data = null;
            return new String[]{status, data};
        } else if (totalTagihan != Integer.parseInt(totalBayar)) {
            String status = "invalid33";
            String data = null;
            return new String[]{status, data};
        } else {
            //System.out.println("inquiryduluya:" + inquiryduluya + "\n");
            int denda_sppt = Integer.parseInt(inquiryduluya[1].substring(242, 253));
            int sppt_dibayar = Integer.parseInt(inquiryduluya[1].substring(254, 265));
            //System.out.println("Denda SPPT:" + denda_sppt);
            //System.out.println("SPPT Dibayar: " + sppt_dibayar);

            try {
                con = DriverManager.getConnection(url, user, password);
                String stm = "INSERT INTO TMP_PEMBAYARAN_SPPT (KD_PROPINSI, KD_DATI2, KD_KECAMATAN, KD_KELURAHAN, KD_BLOK, NO_URUT, KD_JNS_OP, THN_PAJAK_SPPT, PEMBAYARAN_SPPT_KE, KD_KANWIL, KD_KANTOR, KD_TP, DENDA_SPPT, JML_SPPT_YG_DIBAYAR, TGL_PEMBAYARAN_SPPT, TGL_REKAM_BYR_SPPT, NIP_REKAM_BYR_SPPT, PAYMENTPOINTCODE) VALUES (?, ?, ?, ?, ?, ?, ?, ?, '1', '01', '01', 'PA', ?, ?, SYSDATE, SYSDATE, '06008374ISO', ?)";

                pst = con.prepareStatement(stm);

                pst.setString(1, kd_prop);
                pst.setString(2, kd_dati2);
                pst.setString(3, kd_kec);
                pst.setString(4, kd_kel);
                pst.setString(5, kd_blok);
                pst.setString(6, no_urut);
                pst.setString(7, kd_jns_op);
                pst.setString(8, tahun);
                pst.setInt(9, denda_sppt);
                pst.setInt(10, sppt_dibayar);
                pst.setString(11, paymentPointCode);

                int rows = pst.executeUpdate();

                if (rows > 0) {
                    String status = "success";
                    String data = inquiryduluya[1];
                    return new String[]{status, data};
                } else {
                    String status = "noinsert";
                    String data = null;
                    return new String[]{status, data};
                }
            } catch (SQLException ex) {
                Logger lgr = Logger.getLogger(Bantuan.class.getName());
                lgr.log(Level.SEVERE, ex.getMessage(), ex);
                String status = "noinsert";
                String data = null;
                return new String[]{status, data};

            } finally {
                try {
                    if (pst != null) {
                        pst.close();
                    }
                    if (con != null) {
                        con.close();
                    }

                } catch (SQLException ex) {
                    Logger lgr = Logger.getLogger(Bantuan.class.getName());
                    lgr.log(Level.SEVERE, ex.getMessage(), ex);
                }
            }
        }
    }
}