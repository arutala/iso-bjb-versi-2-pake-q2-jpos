/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package arutala.bjb.switching.iso;

import java.io.*;
import java.util.logging.Level;
import org.jpos.iso.*;
import org.jpos.util.*;

/**
 *
 * @author beta
 */
public class ISOServer_BJB implements ISORequestListener {

    public ISOServer_BJB() {
        super();
    }

    public boolean process(ISOSource source, ISOMsg m) {
        try {
            switch (m.getMTI()) {
                case "0800":
                    // Net Sign on Request
                    m.setResponseMTI();
                    // cek dulu field 7, 11 dan 70 ada isinya ndak

                    //m.set(7, Bantuan.saatIni()); // Mandatory, Jam Transaksi (echo saja)
                    m.set(11, m.getString(11)); // Echo, unique number
                    String ResponseField70 = null;
                    Boolean SignOn = false;

                    switch (m.getString(70)) {
                        case "001":
                            //Sign On
                            ResponseField70 = "001";
                            SignOn = true;
                            m.set(39, "00"); // Optional, only successful response
                            m.set(70, ResponseField70); //balas sesuai Response Field hasil switch
                            break;
                        case "002":
                            //Sign Off
                            ResponseField70 = "002";
                            SignOn = false;
                            m.set(39, "00"); // Optional, only successful response
                            m.set(70, ResponseField70); //balas sesuai Response Field hasil switch
                            break;
                        case "301":
                            //Echo Test
                            ResponseField70 = "301";
                            m.set(39, "00"); // Optional, only successful response
                            m.set(70, ResponseField70); //balas sesuai Response Field hasil switch
                            break;
                        default:
                            //request unknown
                            m.set(39, "76"); // Optional, 76 Other Error
                            break;
                    }
                    break;

                case "0200":
                    // financial transactions
                    m.setResponseMTI();
                    //m.set(7, Bantuan.saatIni()); // Mandatory, Jam Transaksi (echo saja)
                    m.set(11, m.getString(11)); // Echo, unique number

                    String nop = m.getString(61).substring(0, 18);
                    String tahun = m.getString(61).substring(18, 22);

                    switch (m.getString(3)) {
                        case "341019":
                            // inquiry
                            String[] hasil = Bantuan.inquiryNOP(nop, tahun);
                            switch (hasil[0]) {
                                case "invalid":
                                    m.set(39, "76"); // NOP invalid
                                    break;
                                case "down":
                                    m.set(39, "91"); // Link Down
                                    break;
                                case "paid":
                                    m.set(39, "54"); // Bill Already Paid
                                    break;
                                case "notfound":
                                    m.set(39, "55"); // Bill not Available
                                    break;
                                case "success":
                                    m.set(39, "00"); // Success
                                    m.set(62, hasil[1]);
                                    break;
                                default:
                                    // unknown
                                    m.set(39, "76"); // Other Error
                                    break;
                            }
                            break;

                        case "541019":
                            // payment
                            String paymentPointCode = m.getString(107);
                            //int totalBayar = Integer.parseInt(m.getString(62).substring(254, 265));
                            String totalBayar = m.getString(62).substring(254, 265);
                            //0080100001
                            hasil = Bantuan.paymentNOP(nop, tahun, totalBayar, paymentPointCode);
                            switch (hasil[0]) {
                                case "invalid":
                                    m.set(39, "76"); // NOP invalid
                                    break;
                                case "invalid33":
                                    m.set(39, "33"); // Invalid data tagihan, total bayar != total tagihan
                                    break;
                                case "down":
                                    m.set(39, "91"); // Link Down
                                    break;
                                case "paid":
                                    m.set(39, "54"); // Bill Already Paid                                    
                                    break;
                                case "notfound":
                                    m.set(39, "55"); // Bill not Available
                                    break;
                                case "success":
                                    m.set(39, "00"); // Success
                                    m.set(62, hasil[1]);
                                    String NTP = Bantuan.saatIni().substring(0, 4) + nop.substring(4, 18);
                                    m.set(47, NTP); // Set Nomor Trx Pemda
                                    break;
                                default:
                                    System.out.println("Hasil[0]:" + hasil[0]);
                                    System.out.println("Hasil[1]:" + hasil[1]);
                                    // unknown
                                    m.set(39, "76"); // Other Error
                                    break;
                            }
                            break;
                    }
                    break;
                default:
                    m.setResponseMTI();
                    m.set(39, "76"); // 76 Other Error
                    break;
            }
            
            source.send(m);
        } catch (ISOException e) {
            //Logger.getLogger(ISOServer_BJB.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        } catch (IOException e) {
            //Logger.getLogger(ISOServer_BJB.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
        return true;

    }
    /*
     public static void main(String[] args) throws Exception {
     // load config file        
     Properties prop = new Properties();
     InputStream input = null;
     int isoPort = 7357;
     int isoConf;

     try {

     input = new FileInputStream("config.isoserver");

     // load a properties file
     prop.load(input);

     // get the property value 
     isoPort = Integer.parseInt(prop.getProperty("port"));

     } catch (IOException ex) {
     ex.printStackTrace();
     } finally {
     if (input != null) {
     try {
     input.close();
     } catch (IOException e) {
     e.printStackTrace();
     }
     }
     }
     //jalankan Server ISO
     Logger logger = new Logger();
     logger.addListener(new SimpleLogListener(System.out));
     ServerChannel channel = new ASCIIChannel(new GenericPackager("isoBJBSpecification.xml"));
     ((LogSource) channel).setLogger(logger, "channel");
     ISOServer server = new ISOServer(isoPort, channel, null);
     server.setLogger(logger, "server");
     server.addISORequestListener(new ISOServer_BJB());
     new Thread(server).start();
     }
     */
}
