# README #

IP Address: 10.0.0.5

Folder: /home/isobjb/ISO_BJB_q2/

Cara menjalankan manual: cd /home/isobjb/ISO_BJB_q2/ && java -jar ISO_BJB_q2.jar

Cara menjalankan yang aman: cd /home/isobjb/ISO_BJB_q2/ && ./reset-isobjb.sh

Cara kompilasi di lokal: Pake Netbeans, Compile aja..

File konfigurasi menggunakan file teks **config.isoserver**.

```
#!txt

# config file utk Server ISO8583 
# dbConf lihat di file Bantuan.java
port=8000
dbConf=2
```

Jadi kalo ngerubah port, tinggal edit file config.isoserver trus jar nya di restart atau jalankan file reset-isopapua.sh.

Aplikasi ini menggunakan Q2, terdiri dari 2 bagian:
1) File konfigurasi di folder deploy, terdiri dari:
    a. 00_logger.xml (utk log semua yg muncul di layar ke file log/q2.log)
    b. 10_channel.xml (utk setting packager)
    c. 20_channeladaptor.xml (utk setting channel adaptor)
2) Listener: class ISOServer_BJB di file ISOServer_BJB.java, mendapat request dari file 20_channeladaptor.xml

  

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact